package net.sf.okapi.filters.openxml;

import java.util.zip.ZipEntry;

import javax.xml.stream.events.XMLEvent;

public class ExcelCommentPartHandler extends StyledTextPartHandler {
	public ExcelCommentPartHandler(ConditionalParameters cparams, OpenXMLZipFile zipFile, ZipEntry entry) {
		super(cparams, zipFile, entry, new EmptyStyleDefinitions());
	}

	@Override
	protected boolean isStyledBlockStartEvent(XMLEvent e) {
		return XMLEventHelpers.isStartElement(e, "text");
	}
}
